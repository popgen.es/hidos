#!/usr/bin/env -S just --justfile

default:
    just --list

test-runtime:
    pytest tests

test:
    ruff check hidos || true
    mypy --strict hidos
    cd tests && mypy --ignore-missing-imports .  # cd for separate mypy cache+config
    pytest tests

test-hidos: test
check: test
check-runtime: test-runtime
